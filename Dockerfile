
FROM python:3.10-alpine3.14
WORKDIR /code
COPY . /code
RUN pip install --upgrade pip && pip install -r requirements.txt 
EXPOSE 8081
CMD ["python3","./Remi.py"]
